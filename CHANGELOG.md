## [1.1.3](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.1.2...v1.1.3) (2020-06-22)


### Bug Fixes

* build package by using MANIFEST.in ([3944cd0](https://gitlab.com/blissfulreboot/python/graafilohi/commit/3944cd05b1e7ffe376e9ac5399594c5b32b337ea))

## [1.1.2](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.1.1...v1.1.2) (2020-06-22)


### Bug Fixes

* fix simulation type and attribute errors ([b835acb](https://gitlab.com/blissfulreboot/python/graafilohi/commit/b835acb39f6a459fe39ad363e0a34d9b8c630f92))

## [1.1.1](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.1.0...v1.1.1) (2020-06-22)


### Bug Fixes

* packaging not including scripts ([0ab5ed3](https://gitlab.com/blissfulreboot/python/graafilohi/commit/0ab5ed3c26abc108a07f3abd1ec414cc3e3576c7))

# [1.1.0](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.0.5...v1.1.0) (2020-06-21)


### Features

* node colors based on whether they are callable ([91aed2e](https://gitlab.com/blissfulreboot/python/graafilohi/commit/91aed2ef307ab0fa5266d045724da1b36eb77e84))

## [1.0.5](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.0.4...v1.0.5) (2020-06-21)


### Bug Fixes

* Fix package ([6498de2](https://gitlab.com/blissfulreboot/python/graafilohi/commit/6498de2b4ebb935f82fc31ec62728d44dc8a8074))

## [1.0.4](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.0.3...v1.0.4) (2020-06-21)


### Bug Fixes

* script renamed to match the package ([85c10ef](https://gitlab.com/blissfulreboot/python/graafilohi/commit/85c10efe4969f9d278617f1fe91863715f707ef5))

## [1.0.3](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.0.2...v1.0.3) (2020-06-20)


### Bug Fixes

* add readme.md to the package_data ([6fb7ff9](https://gitlab.com/blissfulreboot/python/graafilohi/commit/6fb7ff99109d965bbbfc84cf7c3617b89661836c))

## [1.0.2](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.0.1...v1.0.2) (2020-06-20)


### Bug Fixes

* fix installation issue: Add VERSION and changelog.md to the package ([ffb9feb](https://gitlab.com/blissfulreboot/python/graafilohi/commit/ffb9feb5c0d4a1b121e9ce93bed1df51ba5f0284))

## [1.0.1](https://gitlab.com/blissfulreboot/python/graafilohi/compare/v1.0.0...v1.0.1) (2020-06-20)


### Bug Fixes

* fix project name in setup.py ([e5ee4bc](https://gitlab.com/blissfulreboot/python/graafilohi/commit/e5ee4bc7b29082ead27dd55457cc80d25bcc2088))

# 1.0.0 (2020-06-20)


### Features

* initial version of graafilohi ([1e099c4](https://gitlab.com/blissfulreboot/python/graafilohi/commit/1e099c40499678aa19d29dd8e55ee8d1786ad340))
